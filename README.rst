=============
adom-analysis
=============

This is code to scrape the forums for an excellent roguelike video game,
Ancient Domains of Mystery, and generate data and charts about races, classes,
star signs used by players.

Dataset
=======

First, you may just be interested in the data, and not want to run any code.
The data is contained in the repository, in the file "data.tsv". This file is
subject to being updated as I improve the code.

Usage
=====

This code was developed on Arch Linux, but will probably work on OS X, or
Windows using Cygwin, if the proper packages are installed.

Requirements for scraping and generating the dataset:
- basic UNIX tools (bash, sed, awk, grep, etc, should come standard with any UNIX-like environment)
- curl
- lynx

Requirements for generating the figures:
- R (statistical programming language)

To regenerate the dataset by scraping the forums:

.. code-block:: bash
    
    make -B data.tsv

To generate the figures:

.. code-block:: bash

    make

Future directions
=================

The code and dataset right now only includes data from ADOM forums YAVPs. I
want to also add YASDs, and possibly also use data from the Steam version if
there is a way to get that. Probably I will also add a way to automatically
build a nice PDF or HTML version of the analysis from the data, including
commentary, using Sweave or something.

License
=======

Public domain.
