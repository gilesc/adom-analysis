all:
	mkdir -p img
	Rscript analysis.R

data.tsv:
	script/generate-data > $@

.PHONY: all
